
# coding: utf-8

# In[11]:


import numpy as np
import matplotlib.pyplot as plt
import sys
import caffe
caffe_root = './' 


# In[12]:


MODEL_FILE = caffe_root+'placesCNN/places205CNN_deploy.prototxt'
PRETRAINED = caffe_root+'placesCNN/places205CNN_iter_300000.caffemodel'
MEAN_FILE = caffe_root+'placesCNN/places205CNN_mean.binaryproto'
'''MODEL_FILE = caffe_root+'googlenet_placesCNN/places205CNN_deploy.prototxt'
PRETRAINED = caffe_root+'googlenet_placesCNN/googlelet_places205_train_iter_2400000.caffemodel'
MEAN_FILE = caffe_root+'googlenet_placesCNN/places205CNN_mean.binaryproto'''


# In[26]:


imagenet_label_file = caffe_root+'placesCNN/categoryIndex_places205.csv'
labels = list(np.loadtxt(imagenet_label_file, str, delimiter='\,'))
assert len(labels) == 205
print( 'Loaded places205 labels:\n', '\n'.join(labels[:10] + ['...']))'
'''imagenet_label_file = caffe_root+'googlenet_placesCNN/categoryIndex_places205.csv'
labels = list(np.loadtxt(imagenet_label_file, str, delimiter='\,'))
assert len(labels) == 205
print( 'Loaded places205 labels:\n', '\n'.join(labels[:10] + ['...']))''''


# In[14]:


proto_data = open(MEAN_FILE, 'rb').read()
a = caffe.io.caffe_pb2.BlobProto.FromString(proto_data)
mean  = caffe.io.blobproto_to_array(a)[0]


# In[15]:


caffe.set_mode_cpu()


# In[16]:


net = caffe.Classifier(MODEL_FILE, PRETRAINED,
                       mean=mean,
                       channel_swap=(2,1,0),
                       raw_scale=255,
                       image_dims=(256, 256))
print ("successfully loaded classifier")


# In[21]:


# transform it and copy it into the net
#input_image = caffe.io.load_image('image.jpg')
input_image = caffe.io.load_image(caffe_root+'data/dam.jpg')
# test on a image
# predict takes any number of images and formats them for the Caffe net automatically
pred = net.predict([input_image])
net.forward()
output_prob = net.blobs['prob'].data[0]
top_inds = output_prob.argsort()[::-1][:5]

for i in top_inds: print (labels[i])
plt.imshow(input_image)


# In[22]:


output_prob[top_inds],top_inds
for i in top_inds: print (labels[i])


# In[23]:


len(pred[0]),pred


# In[25]:


top_inds,output_prob[top_inds]

